// ESM
import { faker } from "@faker-js/faker";
import fs from "fs";

function createRandomProduct() {
  return {
    id: faker.string.uuid(),
    name: faker.commerce.productName(),
    price: faker.commerce.price(),
    description: faker.commerce.productDescription(),
    product: faker.commerce.product(),
    color: faker.vehicle.color(),
    createdAt: faker.date.past(),
    image: faker.image.avatar(),
  };
}

const PRODUCTS = Array.from({ length: 1000 }, createRandomProduct);

fs.writeFileSync("products.json", JSON.stringify(PRODUCTS, null, 2));
