import {
  getAllProducts,
  addProduct,
  upProduct,
  delProduct,
  getOneProduct,
} from "../../database/productRepository.js";

export const getProducts = (ctx) => {
  try {
    const limit = parseInt(ctx.query.limit);
    const sort = ctx.query.sort;

    const products = getAllProducts({ limit, sort });

    ctx.body = {
      data: products,
    };
  } catch (e) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
};

export const createProduct = (ctx) => {
  try {
    const postData = ctx.request.body;
    postData.id = new Date().getTime();

    addProduct(postData);

    ctx.status = 201;
    return (ctx.body = {
      success: true,
      data: postData,
    });
  } catch (e) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      error: e.message,
    };
  }
};

export const updateProduct = (ctx) => {
  try {
    const product = ctx.request.body;
    product.id = ctx.params.id;

    const updatedProduct = upProduct(product);

    if (!updatedProduct) {
      ctx.status = 404;
      return (ctx.body = {
        success: false,
        message: "Product not found",
      });
    }

    ctx.status = 200;
    return (ctx.body = {
      success: true,
      data: updatedProduct,
    });
  } catch (e) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
};

export const deleteProduct = (ctx) => {
  try {
    const id = ctx.params.id;
    const check = delProduct(id);

    if (!check) {
      ctx.status = 404;
      return (ctx.body = {
        success: false,
        message: "Product not found",
      });
    }

    ctx.status = 204;
    return (ctx.body = null);
  } catch (e) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
};

export const getProduct = (ctx) => {
  try {
    const id = ctx.params.id;
    const fields = ctx.query.fields;
    const product = getOneProduct({ id, fields });

    if (!product) {
      ctx.status = 404;
      return (ctx.body = {
        success: false,
        message: "Product not found",
      });
    }

    ctx.status = 200;
    return (ctx.body = {
      success: true,
      data: product,
    });
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      error: e.message,
    };
  }
};
