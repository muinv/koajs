import fs from "fs";
import data from "./products.json" assert { type: "json" };

export const getAllProducts = ({ limit, sort }) => {
  let products = data;

  if (sort === "desc") {
    products = products.sort(
      (a, b) => new Date(b.createdAt) - new Date(a.createdAt)
    );
  }

  if (sort === "asc") {
    products = products.sort(
      (a, b) => new Date(a.createdAt) - new Date(b.createdAt)
    );
  }

  if (limit) {
    products = products.slice(0, limit);
  }

  return products;
};

export const addProduct = (product) => {
  product.createdAt = new Date().toISOString();
  const newProducts = [product, ...data];
  fs.writeFileSync("./src/database/products.json", JSON.stringify(newProducts));
};

export const upProduct = (product) => {
  try {
    const products = data;

    const index = products.findIndex((p) => p.id === product.id);
    if (index === -1) {
      return null;
    }

    product.updatedAt = new Date().toISOString();
    products[index] = product;
    fs.writeFileSync("./src/database/products.json", JSON.stringify(products));

    return product;
  } catch (error) {
    console.log(error);
  }
};

export const delProduct = (id) => {
  try {
    const products = data;

    const index = products.findIndex((p) => p.id === id);
    if (index === -1) {
      return false;
    }

    products.splice(index, 1);
    fs.writeFileSync("./src/database/products.json", JSON.stringify(products));

    return true;
  } catch (error) {
    console.log(error);
  }
};

export const getOneProduct = ({ id, fields }) => {
  let product = data.find((p) => p.id === id);
  if (fields) {
    const newProduct = {};
    fields.split(",").forEach((field) => {
      newProduct[field] = product[field];
    });
    product = newProduct;
  }
  return product;
};
