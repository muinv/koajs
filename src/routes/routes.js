import Router from "koa-router";

import { productInputMiddleware } from "../middleware/productInputMiddleware.js";
import { productUpdateMiddleware } from "../middleware/productUpdateMiddleware.js";

import {
  getProducts,
  createProduct,
  updateProduct,
  deleteProduct,
  getProduct,
} from "../handlers/products/productHandlers.js";

// Prefix all routes with /products
const router = new Router({
  prefix: "/api",
});

// Routes will go here
router.get("/products", getProducts);
router.post("/products", productInputMiddleware, createProduct);
router.put("/products/:id", productUpdateMiddleware, updateProduct);
router.delete("/products/:id", deleteProduct);
router.get("/products/:id", getProduct);

export { router };
