import * as yup from "yup";

export const productInputMiddleware = async (ctx, next) => {
  try {
    const product = ctx.request.body;
    const schema = yup.object().shape({
      name: yup.string().required(),
      price: yup.number().positive().required(),
      description: yup.string().required(),
      product: yup.string().required(),
      color: yup.string().required(),
      image: yup.string().required(),
    });

    await schema.validate(product);
    next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
};
