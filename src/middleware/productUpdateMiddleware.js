import * as yup from "yup";

export const productUpdateMiddleware = async (ctx, next) => {
  try {
    const product = ctx.request.body;
    const schema = yup.object().shape({
      name: yup.string(),
      price: yup.number().positive(),
      description: yup.string(),
      product: yup.string(),
      color: yup.string(),
      createdAt: yup.string(),
      image: yup.string(),
    });

    await schema.validate(product);
    next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
};
